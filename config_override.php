<?php
/***CONFIGURATOR***/
$sugar_config['http_referer']['actions'][0] = 'index';
$sugar_config['http_referer']['actions'][1] = 'ListView';
$sugar_config['http_referer']['actions'][2] = 'DetailView';
$sugar_config['http_referer']['actions'][3] = 'EditView';
$sugar_config['http_referer']['actions'][4] = 'Login';
$sugar_config['http_referer']['actions'][5] = 'index';
$sugar_config['http_referer']['actions'][6] = 'ListView';
$sugar_config['http_referer']['actions'][7] = 'DetailView';
$sugar_config['http_referer']['actions'][8] = 'EditView';
$sugar_config['http_referer']['actions'][9] = 'oauth';
$sugar_config['http_referer']['actions'][10] = 'authorize';
$sugar_config['http_referer']['actions'][11] = 'Authenticate';
$sugar_config['http_referer']['actions'][12] = 'Login';
$sugar_config['http_referer']['actions'][13] = 'SupportPortal';
$sugar_config['http_referer']['actions'][14] = 'SetTimezone';
$sugar_config['session_dir'] = 'sess/';
$sugar_config['default_currency_iso4217'] = 'PLN';
$sugar_config['default_currency_name'] = 'PLN';
$sugar_config['default_currency_symbol'] = 'zł';
$sugar_config['default_time_format'] = 'H.i';
$sugar_config['passwordsetting']['SystemGeneratedPasswordON'] = '0';
$sugar_config['passwordsetting']['generatepasswordtmpl'] = '';
$sugar_config['passwordsetting']['lostpasswordtmpl'] = '';
$sugar_config['SAML_loginurl'] = '';
$sugar_config['SAML_X509Cert'] = '';
$sugar_config['authenticationClass'] = '';
/***CONFIGURATOR***/